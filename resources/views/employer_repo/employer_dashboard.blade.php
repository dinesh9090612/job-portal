@extends("layout.employer_dashboard_layout")
@push('css')
<link rel="stylesheet" href="{{asset('/css/employer_dashboard.css')}}">
@endpush

@section('content')
<h1>Job Portal</h1>
<h1>Hi, {{Session::get('employer_first_name')}}</h1>
@endsection