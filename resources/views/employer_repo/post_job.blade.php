@extends("layout.employer_dashboard_layout")
@push('css')
<link rel="stylesheet" href="{{asset('/css/job_post.css/')}}">
@endpush
@section('content')
<form action="" method="post" class="employer_job_post_form">


    @csrf
    <table>
        <tr>
            <td> <label for="jobTitle">Job Title</label></td>
            <td> <input type="text" name="job_title" id="jobTitle"> </td>
            <td>
                @error('job_title')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td> <label for="jobDescription">Description</label></td>
            <td>
                <textarea name="job_description" id="jobDescription" cols="30" rows="10"></textarea>
            </td>
            <td>
                @error('job_description')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td>
                <label for="budget">Budget(CTC)</label>
            </td>
            <td>
                <input type="text" name="budget" id="budget">

            </td>
            <td>
                @error('budget')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td> <label for="">Job Type</label></td>
            <td>
                <select name="job_type_id" id="">
                    <option value="">Job Type</option>
                    @foreach($job_types as $job_type)
                    <option value="{{$job_type['id']}}">{{$job_type['job_type']}}</option>
                    @endforeach

                </select>
            </td>
        </tr>
        <tr>
            <td> <label for="">Job Location</label></td>
            <td>
                <select name="location_id" id="">
                    <option value="">Location</option>
                    @foreach($location_composer as $location)
                    <option value="{{$location['id']}}">{{ $location['city'] }}</option>
                    @endforeach
                </select>
            </td>
            <td>
              
                <input type="hidden" name="employer_id" value="{{Session::get('employer_id')}}">
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="POST">
            </td>
        </tr>
        <tr>
        </tr>
    </table>

</form>
@endSection