@extends("layout.employer_dashboard_layout")
@push('css')
<link rel="stylesheet" href="{{asset('/css/show_jobs.css')}}">
@endpush
@section('content')

<h1>My Jobs</h1>
<table>
    <tr>
        <th>Job Title</th>
        <th>Job Location</th>
        <th>Max CTC Budget</th>
        <th>Job Type</th>
        <th>Applications</th>
        <th>Actions</th>
    </tr>

    @foreach($jobs as $job)
    <tr>

        <td>
            <a href=""> {{$job['job_title']}} </a>
        </td>
        <td>
            {{$job->location->city}}
        </td>
        <td>
            {{$job['budget']}}
        </td>
        <td>
            {{$job->job_type->job_type}}
        </td>
        <td>
            <a href="{{route('showCandidates',['id'=>$job->id])}}">
                {{$job->candidate_count}}
            </a>
        </td>
        <td>

            <a href="">Edit</a>
            <a href="">Delete</a>
        </td>
    </tr>
    @endforeach
</table>
@endsection