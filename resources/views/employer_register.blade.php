@extends('layout.master')
@section('content')
<h1>Employee Sign Up</h1>
<form action="{{route('employer.register')}}" method="POST">
    @csrf
    <table>
        <tr>
            <td>Company Name</td>
            <td> <input type="text" name="company_name" id=""> </td>
            <td>
                @error('company_name')
                {{$message}}
                @enderror
            </td>
        </tr>

        <tr>
            <td>First Name </td>
            <td><input type="text" name="first_name" id=""></td>
            <td>
                @error('first_name')
                {{$message}}
                @enderror
            </td>
        </tr>

        <tr>
            <td>Last Name</td>
            <td><input type="text" name="last_name" id=""></td>
            <td>
                @error('last_name')
                {{$message}}
                @enderror
            </td>
        </tr>

        <tr>
            <td>Gender</td>
            <td>
                <input type="radio" name="gender" value="male" id="">Male
                <input type="radio" name="gender" value="female" id="">Female
            </td>

        </tr>

        <tr>
            <td>Contact Number</td>
            <td><input type="text" name="contact_number" id=""></td>
            <td>
                @error('contact_number')
                {{$message}}
                @enderror
            </td>
        </tr>

        <tr>
            <td>Current Location</td>
            <td>
                <select name="location_id" id="">
                    <option value="">Select City</option>
                    @foreach($location_composer as $location)
                    <option value="{{$location->id}}">{{$location->city}}</option>
                    @endforeach
                </select>
            </td>
        </tr>
        <tr>
            <td>Email id</td>
            <td><input type="text" name="email_address" id=""></td>
            <td>
                @error('email_address')
                {{$message}}
                @enderror
            </td>

        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password" id=""></td>
            <td>

                @error('password')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td>Re-enter Password</td>
            <td><input type="password" name="password_confirmation" id=""></td>
            <td>

                @error('confirm_password')
                {{$message}}
                @enderror

            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="Submit">
            </td>
        </tr>
    </table>

</form>
@endsection