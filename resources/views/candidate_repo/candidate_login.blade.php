@extends('layout.master')
@section('content')
<form action="{{route('candidate.loginValidation')}}" method="post">
    @csrf
    <table>
        <tr>
            <td>
                <label for="email_address">
                    Email Address
                </label>
            </td>
            <td>
                <input type="email" name="email" id="email_address">
            </td>
            <td>
                @error('email')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td>
                <label for="password">
                    Password
                </label>
            </td>
            <td>
                <input type="password" name="password" id="password">
            </td>
            <td>
                @error('password')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="Submit">
            </td>
            <td>
                <button>
                    <a href="{{route('candidate.register')}}">Sign Up</a>
                </button>
            </td>
        </tr>
    </table>
</form>
@endsection