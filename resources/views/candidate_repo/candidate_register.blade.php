@extends('layout.master')

@section('content')
<form action="{{url('jobportal/candidate/register')}}" method="post">
    @csrf
    <table>
        <tr>
            <td><label for="current_ctc">Current CTC</label> </td>
            <td><input type="text" name="current_ctc" id="current_ctc" value="{{old('current_ctc')}}"></td>
            <td>
                @error('current_ctc')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td><label for="first_name">First Name</label></td>
            <td><input type="text" name="first_name" id="first_name" value="{{old('first_name')}}"></td>
            <td>
                @error('first_name')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td><label for="last_name">Last Name</label></td>
            <td><input type="text" name="last_name" id="last_name" value="{{old('last_name')}}"></td>
            <td>
                @error('last_name')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td><label for="">Gender</label></td>
            <td><input type="radio" name="gender" value="male">Male </td>
            <td><input type="radio" name="gender" value="female">Female</td>
            <td>
                @error('gender')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td><label for="date_of_birth">Date Of Birth</label>
            <td><input type="date" name="date_of_birth" id="date_of_birth">
            <td>
                @error('date_of_birth')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td><label for="contact_number">Contact Number</label>
            <td><input type="text" name="contact_number" id="contact_number" value="{{old('contact_number')}}"></td>
            <td>
                @error('contact_number')
                {{$message}}
                @enderror
            </td>

        </tr>
        <tr>
            <td><label for="">Current Location</label>
            <td><select name="location_id" id="">
                    <option value="">Select Location</option>
                    @foreach($location_composer as $location)
                    <option value="{{$location->id}}">{{$location->city}}</option>
                    @endforeach
                </select>
</td>
<td>
    @error('location_name')
    {{$message}}
    @enderror
</td>
        </tr>
        <tr>
            <td><label for="email">Email Id</label>
            <td><input type="email" name="email_address" id="email" value="{{old('email_address')}}">
            <td>
                @error('email_address')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td><label for="password">Password</label>
            <td><input type="password" name="password" id="password">
            <td>
                @error('password')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td><label for="password_confirmation">Re-Enter Password</label>
            <td><input type="password" name="password_confirmation" id="password_confirmation">
            <td>
                @error('password_confirmation')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="Sign Up">
        </tr>
    </table>

</form>
@endsection