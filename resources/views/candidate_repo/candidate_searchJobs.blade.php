@extends('layout.candidate_dashboard_layout')
@section('content')
<div>
    <h2>Job Search </h2>
    <form action="{{route('fetchJobs')}}" method="post">
        @csrf
        <select name="location_id" id="">
            <option value="">Select Location</option>
            @foreach($location_composer as $location)
            <option value="{{$location['id']}}">{{$location['city']}}</option>
            @endforeach
        </select>
        <span>
            @error('location_id')
            {{$message}}
            @enderror
        </span>

        <select name="job_type_id" id="">
            <option value="">Select Job Type</option>
            @foreach($job_types as $job)
            <option value="{{$job['id']}}">{{$job['job_type']}}</option>
            @endforeach
        </select>
        <span>
            @error('job_type_id')
            {{$message}}
            @enderror
        </span>
        <input type="submit" value="Search">
    </form>
</div>
<div>
    <h2>Search Results</h2>
</div>
@endsection