@extends('layout.candidate_dashboard_layout')
@push('css')
<link rel="stylesheet" href="{{asset('/css/candidateDisplayJobs.css')}}">
@endpush
@section('content')
<table>
    <tr>
        <th>Job Id</th>
        <th>Job Title</th>
        <th>Comapny Name</th>
        <th>City</th>
    </tr>
    @foreach($jobs as $job)
    <tr>
        <td>{{$job->id}}</td>
        <td>
            <a href="{{route('displayJobDetails',['id'=>$job->id])}}">
                {{$job->job_title}}
            </a>

        </td>
        <td>
            {{$job->employer->company_name}}
        </td>
        <td>
            {{$job->location->city}}
        </td>
    </tr>
    @endforeach
</table>
@endsection