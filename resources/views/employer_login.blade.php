@extends('layout.master')

@section('content')
<form action="{{route('employer.login')}}" method="POST">

    @csrf
    <table>

        <tr>
            <td>
                <label for="">Username</label>
            </td>
            <td>
                <input type="text" name="email_address" placeholder="email_address">
            </td>
            <td>

                @error('email_address')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td>
                <label for="">Password</label>
            </td>
            <td>
                <input type="password" name="password" id="">
            </td>
            <td>

                @error('password')
                {{$message}}
                @enderror
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="Login" name="loginBtn">
            </td>
        </tr>
        <tr>
            <td>
                <button>
                    <a href="{{url('/jobportal/employer/register')}}">Sign Up</a>
                </button>
            </td>
        </tr>
    </table>
</form>
@endsection