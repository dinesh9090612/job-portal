<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    @stack('css')
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">

</head>

<body>
    <nav>
        <ul>
            <li><a href="{{route('employer.dashboard')}}">Dashboard</a></li>
            <li><a href="{{route('postJob')}}">Post New Job</a></li>
            <li><a href="{{route('employer.showJob')}}">My Jobs</a></li>
            <li><a href="">My Profile</a></li>
        </ul>
    </nav>

    <div>
        @yield('content')
    </div>

</body>

</html>