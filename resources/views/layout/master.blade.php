<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <title>
        @yield('title')
    </title>
</head>

<body>

    <nav>
        <ul>
            <li> <a href="{{url('/')}}">Home</a> </li>
            <li> <a href="{{url('jobPortal/login')}}">Log In</a></li>
            <li> <a href="{{url('jobPortal/register/')}}">Sign Up</a></li>
            <li> <a href="">Contact Us</a></li>
        </ul>
    </nav>

    <h1>
        @yield('heading')
    </h1>

    <div>
        @yield('content')
    </div>

    <!-- <footer>
        <h1>This is Footer</h1>
    </footer> -->


</body>

</html>