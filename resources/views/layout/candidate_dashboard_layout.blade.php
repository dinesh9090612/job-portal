<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    @stack('css')
    <link rel="stylesheet" href="{{asset('/css/candidate_dashboard.css')}}">

</head>

<body>

    <nav>
        <ul>
            <li><a href="{{route('candidateDashboard')}}">Dashboard</a></li>
            <li><a href="{{route('searchJobs')}}">Search Jobs</a></li>
            <li><a href="{{route('myAppliedJobs')}}">My Applied Jobs</a></li>
            <li><a href="">My Profile</a></li>
        </ul>
    </nav>

    <div>
        @yield('content')
    </div>
</body>

</html>