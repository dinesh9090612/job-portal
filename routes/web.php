<?php

use App\Http\Controllers\CandidateController;
use App\Http\Controllers\EmployerController;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('layout.master');
});

Route::get('jobPortal/register/', function () {
    return view('global_register');
});

Route::get('jobPortal/login/', function () {
    return view('global_login');
});

Route::get('jobportal/employer/register', function () {
    return view('employer_register');
});

Route::post('jobportal/employer/register', [EmployerController::class, 'store'])->name('employer.register');

Route::get('jobportal/employer/login', function () {
    return view('employer_login');
});

Route::post('jobportal/employer/login', [EmployerController::class, 'login'])->name('employer.login');

Route::get('jobportal/employer/dashboard', [EmployerController::class, 'dashboard'])->name('employer.dashboard');

Route::get('jobportal/job/post', [EmployerController::class, 'postJobView']);

Route::post('jobportal/job/post', [EmployerController::class, 'postJob'])->name('postJob');

Route::get('jobportal/employer/my_jobs', [EmployerController::class, 'showJobs'])->name('employer.showJob');

Route::get('jobportal/job/showApplications/{id}', [EmployerController::class, 'showApplication'])->name('showCandidates');

Route::get('jobportal/candidate/register', function () {
    return view('candidate_repo.candidate_register');
});
Route::post('jobportal/candidate/register', [CandidateController::class, 'candidateStore'])->name('candidate.register');

Route::get('jobportal/candidate/login', [CandidateController::class, 'candidateLogin'])->name('candidate.login');
Route::post('jobportal/candidate/login', [CandidateController::class, 'candidateLoginValidation'])->name('candidate.loginValidation');

Route::get('jobportal/candidate/dashboard', [CandidateController::class, 'candidateDashboard'])->name('candidateDashboard');

Route::get('jobportal/candidate/showJobs', [CandidateController::class, 'searchJobs'])->name('searchJobs');
Route::post('jobportal/candidate/show', [CandidateController::class, 'search'])->name('fetchJobs');

Route::get('jobPortal/candidate/display/job/{id}', [CandidateController::class, 'jobDetails'])->name('displayJobDetails');

Route::get('jobPortal/candidate/apply/job/{id}', [CandidateController::class, 'jobApply'])->name('applyJob');
Route::get('jobPortal/candidate/myAppliedJobs', [CandidateController::class, 'showApplications'])->name('myAppliedJobs');