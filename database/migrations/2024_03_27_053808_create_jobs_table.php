<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('job_title');
            $table->string('job_description');
            $table->float('budget');
            $table->unsignedInteger('job_type_id');
            $table->unsignedInteger('employer_id');
            $table->unsignedInteger('location_id');
            $table->foreign('job_type_id')->references('id')->on('job_types');
            $table->foreign('employer_id')->references('id')->on('employers');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jobs');
    }
};
