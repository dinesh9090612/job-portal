<?php

namespace App\Http\Controllers;

use App\Http\Requests\CandidateLoginRequest;
use App\Http\Requests\CandidateStoreRequest;
use App\Http\Requests\JobSearchRequest;
use App\Models\Job;
use App\Models\Candidate;

use App\Models\CandidateJob;

use Illuminate\Support\Facades\Session;

class CandidateController extends Controller
{

    public function candidateStore(CandidateStoreRequest $reqeust)
    {
        $validatedCandidate = $reqeust->validated();
        Candidate::create($validatedCandidate);
        return redirect()->route('candidate.login');
    }
    public function candidateLogin()
    {
        return view('candidate_repo.candidate_login');
    }
    public function candidateLoginValidation(CandidateLoginRequest $request)
    {
        $validated = $request->validated();
        $data = Candidate::where(['email_address' => $validated['email'], 'password' => $validated['password']])->first();
        if (isset($data)) {
            Session::put('candidate_email', $validated['email']);
            Session::put('candidate_first_name', $data['first_name']);
            Session::put('candidate_id', $data['id']);
            return redirect()->route('candidateDashboard');
        } else {
            return redirect()->route('candidate.register');
        }
    }
    public function candidateDashboard()
    {

        return view('candidate_repo.candidate_dashboard');
    }
    public function searchJobs()
    {
        return view('candidate_repo.candidate_searchJobs');
    }
    public function search(JobSearchRequest $request)
    {
        $validatedLocation = $request->all();
        $jobs =  Job::with('employer')->withWhereHas('job_type', function ($query)  use ($validatedLocation) {
            $query->where('id', $validatedLocation['job_type_id']);
        })->withWhereHas('location', function ($query) use ($validatedLocation) {
            $query->where('id', $validatedLocation['location_id']);
        })->get();

        return view('candidate_repo.candidate_displayJobs')->with('jobs', $jobs);
    }

    public function jobDetails($res)
    {
        // dd($res);
        $job =  Job::with('employer', 'location')->find($res);
        echo $job->id . "<br>";
        echo $job->job_title . "<br>";
        echo $job->job_description . "<br>";
        echo $job->location->city . "<br>";
        echo $job->budget . "<br>";
        echo $job->employer->company_name . "<br>";
        echo $job->employer->first_name . "<br>";
        return redirect()->route('applyJob', ['id' => $job->id]);
    }

    public function jobApply($id)
    {

        CandidateJob::create([
            'candidate_id' => Session::get('candidate_id'),
            'job_id' => $id
        ]);

        return "You Applied Succeffully";
    }
    public function showApplications()
    {
        $myAppliedJobs = Candidate::with('jobs')->get();
        dd($myAppliedJobs);
        return view('candidate_repo.candidate_myAppliedJobs', ['appliedJobs' => $myAppliedJobs]);
    }
}
