<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployerLoginRequest;
use App\Http\Requests\EmployerRequest;
use App\Http\Requests\JobPostRequest;
use App\Models\Employer;
use App\Models\Location;
use App\Models\Job;
use App\Models\JobType;
use Illuminate\Support\Facades\Session;

use Locale;

class EmployerController extends Controller
{


    public function store(EmployerRequest $request)
    {
        $employerData = $request->validated();
        Employer::create($employerData);
        return redirect('jobportal/employer/login');
    }

    public function login(EmployerLoginRequest $request)
    {
        $emp =  Employer::where(['email_address' => $request->email_address, 'password' => $request->password])->first();
        if (isset($emp)) {
            Session::put('employer_id', $emp->id);
            Session::put('employer_email_address', $request->email_address);
            Session::put('employer_first_name', $emp->first_name);
            return redirect('jobportal/employer/dashboard');
        } else {
            return "Data with provided Credentials Not Found";
        }
    }

    public function dashboard()
    {
        return view('employer_repo.employer_dashboard');
    }


    public function postJobView()
    {
        return view('employer_repo.post_job');
    }

    public function postJob(JobPostRequest $request)
    {
        $jobData = $request->validated();
        Job::create($jobData);
        return redirect('jobportal/employer/my_jobs');
    }

    public function showJobs()
    {
        $employer = Job::with('job_type', 'location')->withCount('candidate')->where('employer_id', Session::get('employer_id'))->get();
        return view('employer_repo.show_jobs', ['jobs' => $employer]);
    }

    public function showApplication($id)
    {

        return Job::with('candidate')->find($id);
    }
}
