<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CandidateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'first_name'=>'required',
            'last_name'=>'required',
            'current_ctc'=>'required|numeric',
            'gender'=>'required',
            'date_of_birth'=>'required|date',
            'contact_number'=>'required|numeric|digits:10',
            'email_address'=>'required|email:rfc,dns',
            'location_id'=>'required',
            'password'=>'required|min:8|confirmed',
            'password_confirmation'=>'required'
        ];
    }
}
