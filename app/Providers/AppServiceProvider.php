<?php

namespace App\Providers;

use App\Models\JobType;
use Illuminate\Support\ServiceProvider;
use App\Models\Location;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
        View::composer('*', function ($view) {
            $location = Location::all();
            $view->with('location_composer', $location);
        });

        View::composer('*', function ($view) {
            $jobType = JobType::all();
            $view->with('job_types', $jobType);
        });
    }
}
