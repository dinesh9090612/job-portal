<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Job extends Model
{
    use HasFactory;
    protected $table = "jobs";

    public function job_type(): BelongsTo
    {
        return $this->belongsTo(JobType::class);
    }
    public function location(): BelongsTo
    {
        return $this->belongsTo(Location::class);
    }
    public function employer(): BelongsTo
    {
        return $this->belongsTo(Employer::class);
    }
    public function candidate(): BelongsToMany
    {
        return $this->belongsToMany(Candidate::class, 'candidate_jobs', 'job_id', 'candidate_id');
    }
    protected $fillable = [
        'job_title',
        'job_description',
        'budget',
        'job_type_id',
        'employer_id',
        'location_id',
    ];
}
