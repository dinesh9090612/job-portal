<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Location extends Model
{
    use HasFactory;
    protected $table = "locations";
    public $timestamps=false;

    public function candidates(): HasMany
    {
        return $this->hasMany(Candidate::class);
    }
    public function jobs(): HasMany
    {
        return $this->hasMany(Job::class);
    }
    public function employers(): HasMany
    {
        return $this->hasMany(Employer::class);
    }
    protected  $fillable = [
        'id',
        'city',
        'state',
        'created_at',
        'updated_at'
    ];
}
