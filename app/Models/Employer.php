<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Employer extends Model
{
    use HasFactory;
    protected $table = "employers";
    public function location(): BelongsTo
    {
        return $this->belongsTo(Location::class);
    }
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }
    protected  $fillable = [
        'company_name',
        'first_name',
        'last_name',
        'gender',
        'contact_number',
        'location_id',
        'email_address',
        'password',
    ];
}
