<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CandidateJob extends Model
{
    use HasFactory;
    protected $table = "candidate_jobs";

    protected $fillable = [
        'candidate_id',
        'job_id'
    ];
    public  $timestamps=false;
    // public function candidate()
    // {
    //     return $this->belongsToMany(Candidate::class);
    // }
    // public function job()
    // {
    //     return $this->belongsToMany(Job::class);
    // }
}
