<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Candidate extends Model
{
    use HasFactory;
    protected $table = "candidates";
    public function locations(): BelongsTo
    {
        return $this->belongsTo(Location::class);
    }
    public function jobs(): BelongsToMany
    {
        return $this->belongsToMany(Job::class, 'candidate_jobs', 'candidate_id', 'job_id');
    }

    protected $fillable = [
        'first_name',
        'last_name',
        'gender',
        'date_of_birth',
        'contact_number',
        'email_address',
        'password',
        'candidate_image',
        'candidate_resume',
        'location_id',
    ];
}
